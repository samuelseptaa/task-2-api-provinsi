<div class="row m-3">
    <div class="col-sm-6">
        <button type="button" id="tambahProvinsi" class="btn btn-primary btn-xl">Tambah Provinsi</button>
    </div>
    <div class="col-sm-6">
        <div class="text-end">
            <button type="button" id="getProvinsi" class="btn btn-primary btn-xl">Get Provinsi from API</button>
        </div>
    </div>

</div>
<div class="table">
    <table class="table table-responsive table-bordered ">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Key</th>
                <th scope="col">Value</th>
                <th scope="col">File</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 0;
            foreach ($provinsi as $p) :
                $i++; ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $p->key ?></td>
                    <td><?= $p->value ?></td>
                    <td class="text-center"><?php if ($p->file != null) : ?><img src="<?= base_url() . 'public/uploads/' . $p->file ?>" height="300px" width="400px">
                        <?php else : ?>
                            Tidak ada file
                        <?php endif; ?></td>
                    <td>
                        <button type="button" onclick="edit(<?= $p->id ?>)" class="btn btn-primary btn-sm">Edit</button>
                        <button type="button" onclick="deletes(<?= $p->id ?>)" class="btn btn-danger btn-sm">Hapus</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_edit" tabindex="-1" aria-labelledby="modal_editLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_editLabel">Edit Provinsi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="" id="edit">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group mb-3">
                        <label class="col-form-label">Key</label>
                        <input required type="text" id="key" name="key" class="form-control" placeholder="Key">
                    </div>
                    <div class="form-group mb-3">
                        <label class="col-form-label">Nama Provinsi</label>
                        <input required type="text" id="value" name="value" class="form-control" placeholder="Nama Provinsi">
                    </div>
                    <div class="form-group mb-3">
                        <label class="col-form-label">FIle Foto</label>
                        <input type="file" id="file" name="file" accept=".jpg,.png.,jpeg" class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_tambah" tabindex="-1" aria-labelledby="modal_tambahLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_tambahLabel">Tambah Provinsi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="" id="tambah">
                    <div class="form-group mb-3">
                        <label class="col-form-label">Key</label>
                        <input required type="text" id="key__" name="key__" class="form-control" placeholder="Key">
                    </div>
                    <div class="form-group mb-3">
                        <label class="col-form-label">Nama Provinsi</label>
                        <input required type="text" id="value__" name="value__" class="form-control" placeholder="Nama Provinsi">
                    </div>
                    <div class="form-group mb-3">
                        <label class="col-form-label">FIle Foto</label>
                        <input type="file" id="file__" name="file__" accept=".jpg,.png.,jpeg" class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save Data</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url() ?>public/js/jquery.min.js"></script>
<script src="<?= base_url() ?>public/sweetalert2/dist/sweetalert2.all.js"></script>
<script src="<?= base_url() ?>public/sweetalert2/dist/sweetalert2.css"></script>
<script>
    $("#getProvinsi").click(function(e) {
        $.ajax({
            type: "POST",
            url: 'https://kipi.covid19.go.id/api/get-province',
            success: function(response) {
                console.log(response);
                $.ajax({
                    type: "POST",
                    data: response,
                    processData: true,
                    dataType: 'JSON',
                    url: '<?= base_url() ?>index/InsertProvinsi',
                    success: function(response) {
                        Swal.fire({
                            icon: 'success',
                            title: response.message.title,
                            text: response.message.body,
                        }).then((result) => {
                            if (result.isConfirmed) location.reload();
                        });
                    },
                    error: function(jqXHR, textstatus, errorThrown) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: textstatus,
                        });
                    }
                });
            },
            error: function(jqXHR, textstatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: textstatus,
                });
            }
        });
    });

    function edit(id) {
        $.ajax({
            type: "GET",
            data: {
                id: id
            },
            url: '<?= base_url() ?>index/provinsi_get',
            success: function(response) {
                console.log(response);
                const provinsi = response.data.provinsi;
                $("#id").val(provinsi.id);
                $("#key").val(provinsi.key);
                $("#value").val(provinsi.value);
            },
            error: function(jqXHR, textstatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: textstatus,
                });
            }
        });
        $("#modal_edit").modal('show');
    }

    $("#edit").submit(function(e) {
        e.preventDefault();
        let formdata = new FormData(this);
        $.ajax({
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            url: '<?= base_url() ?>index/editProvinsi',
            success: function(response) {
                Swal.fire({
                    icon: 'success',
                    title: response.message.title,
                    text: response.message.body,
                }).then((result) => {
                    if (result.isConfirmed) location.reload();
                });
            },
            error: function(jqXHR, textstatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: textstatus,
                });
            }
        });
    });

    function deletes(id) {
        Swal.fire({
            icon: 'warning',
            text: 'Apakah Anda ingin menghapus data ini?',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            denyButtonText: `Batal`,
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    data: {
                        id: id
                    },
                    processData: true,
                    dataType: 'JSON',
                    url: '<?= base_url() ?>index/hapus_provinsi',
                    success: function(response) {
                        Swal.fire({
                            icon: 'success',
                            title: response.message.title,
                            text: response.message.body,
                        }).then((result) => {
                            if (result.isConfirmed) location.reload();
                        });
                    },
                    error: function(jqXHR, textstatus, errorThrown) {
                        Swal.fire({
                            icon: 'error',
                            title: textstatus,
                            text: jqXHR.responseJSON.message.body,
                        });
                    }
                });
            }
        });
    }

    $("#tambahProvinsi").click(function(e) {
        $("#modal_tambah").modal('show');
    });

    $("#tambah").submit(function(e) {
        let formdata = new FormData(this);
        $.ajax({
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            url: '<?= base_url() ?>index/addProvinsi',
            success: function(response) {
                Swal.fire({
                    icon: 'success',
                    title: response.message.title,
                    text: response.message.body,
                }).then((result) => {
                    if (result.isConfirmed) location.reload();
                });
            },
            error: function(jqXHR, textstatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: textstatus,
                    text: jqXHR.responseJSON.message.body,
                });
            }
        });
    })
</script>