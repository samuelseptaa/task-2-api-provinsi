<?php
defined('BASEPATH') or exit('No direct script access allowed');
class M_Provinsi extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function create($data)
    {
        $created = 0;
        $alreadythere = 0;
        foreach ($data as $d) {
            $isThere = $this->db->get_where('provinsi', ['key' => $d['key']])->row();
            if ($isThere == null) {
                $this->db->insert('provinsi', $d);
                $created++;
            } else $alreadythere++;
        }
        return [
            'created' => $created,
            'alreadythere' => $alreadythere
        ];
    }

    public function get($select = null, $where = null, $limit = null)
    {
        if ($select != null)
            $this->db->select($select);
        else
            $this->db->select('*');

        if ($where != null)
            $this->db->where($where);

        if ($limit != null)
            $this->db->limit($limit);

        $this->db->order_by('key', 'asc');
        $result = $this->db->get('provinsi');
        if ($limit == 1) {
            return $result->row();
        }
        return $result->result();
    }

    public function createorExist($data)
    {
        $isThere = $this->db->get_where('provinsi', ['key' => $data['key__']])->row();
        if ($isThere == null) {
            $this->db->insert('provinsi', [
                'key' => $data['key__'],
                'value' => $data['value__'],
                'file' => $data['file__'],
            ]);
            return true;
        }
        return false;
    }
}
