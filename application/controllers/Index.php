<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Index extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_Provinsi', 'provinsi');
    }

    public function index()
    {
        $data['provinsi'] = $this->provinsi->get('*');
        $this->renderTo('index', $data);
    }

    private function curl_test()
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://kipi.covid19.go.id/api/get-province",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'],

        ));
        $response = curl_exec($curl);

        $value = json_decode($response);
        echo ($value);
    }

    public function InsertProvinsi()
    {
        $data = $this->input->post();
        if ($data == null) {
            return $this->responseJSON(
                405,
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Error',
                        'body' => "Method not Allowed"
                    ]
                ]
            );
        }

        $provinsi = $data['results'];
        $result = $this->provinsi->create($provinsi);
        return $this->responseJSON(
            200,
            [
                'status' => 'OK',
                'message' => [
                    'title' => 'Berhasil',
                    'body' => $result['created'] . " data ditambahkan, " . $result['alreadythere'] . " sudah ada"
                ]
            ]
        );
    }

    public function provinsi_get()
    {
        $data = $this->input->get();

        $provinsi = $this->provinsi->get('*', ['id' => $data['id']], 1);
        if ($provinsi != null)
            return $this->responseJSON(
                200,
                [
                    'status' => 'OK',
                    'message' => [
                        'title' => 'Berhasil',
                        'body' => "Data ditemukan"
                    ],
                    'data' => [
                        'provinsi' => $provinsi
                    ]
                ]
            );
        else {
            return $this->responseJSON(
                404,
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Error',
                        'body' => "Data dengan id tersebut tidak ditemukan"
                    ]
                ]
            );
        }
    }

    public function addProvinsi()
    {
        $data = $this->input->post();
        if ($data == null) {
            return $this->responseJSON(
                405,
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Error',
                        'body' => "Method not Allowed"
                    ]
                ]
            );
        }
        $data['file__'] = null;
        if (!empty($_FILES['file__']['name'])) {
            $responseupload = $this->do_upload('file__', str_replace(" ", "_", $data['key__']));

            if ($responseupload['status'])
                $data['file__'] = $responseupload['file_name'];
            else {
                return $this->responseJSON(
                    500,
                    [
                        'status' => 'Error',
                        'message' => [
                            'title' => 'Gagal',
                            'body' => $responseupload['error']
                        ]
                    ]
                );
            }
        }

        $isCreated = $this->provinsi->createorExist($data);
        if ($isCreated) {
            return $this->responseJSON(
                200,
                [
                    'status' => 'OK',
                    'message' => [
                        'title' => 'Berhasil',
                        'body' => "Data ditambahkan"
                    ],
                ]
            );
        }
        return $this->responseJSON(
            500,
            [
                'status' => 'Error',
                'message' => [
                    'title' => 'Gagal',
                    'body' => 'Data dengan key tersebut sudah ada!'
                ]
            ]
        );
    }


    public function editProvinsi()
    {
        $data = $this->input->post();
        if ($data == null) {
            return $this->responseJSON(
                405,
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Error',
                        'body' => "Method not Allowed"
                    ]
                ]
            );
        }
        if (!empty($_FILES['file']['name'])) {
            $responseupload = $this->do_upload('file', str_replace(" ", "_", $data['key']));

            if ($responseupload['status'])
                $data['file'] = $responseupload['file_name'];
            else {
                return $this->responseJSON(
                    500,
                    [
                        'status' => 'Error',
                        'message' => [
                            'title' => 'Gagal',
                            'body' => $responseupload['error']
                        ]
                    ]
                );
            }
        }
        $this->db->update('provinsi', $data, ['id' => $data['id']]);
        return $this->responseJSON(
            200,
            [
                'status' => 'OK',
                'message' => [
                    'title' => 'Berhasil',
                    'body' => "Data diubah"
                ],
            ]
        );
    }

    private function do_upload($file, $filename)
    {
        $config = array(
            'upload_path' => './public/uploads',
            'file_name' => date('His') . '__' . $filename . '.jpg',
            'allowed_types' => "jpg|png|jpeg|JPG",
            'overwrite' => TRUE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
        );

        $this->load->library('upload', $config);
        if ($this->upload->do_upload($file)) {
            $data = array('upload_data' => $this->upload->data());
            return [
                'status' => true,
                'file_name' => $data['upload_data']['file_name']
            ];
        } else {
            $error = array('error' => $this->upload->display_errors());
            return [
                'status' => false,
                'error' => $error
            ];
        }
    }

    public function hapus_provinsi()
    {
        $data = $this->input->post();
        if ($data == null) {
            return $this->responseJSON(
                405,
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Error',
                        'body' => "Method not Allowed"
                    ]
                ]
            );
        }

        $this->db->delete('provinsi', $data);
        return $this->responseJSON(
            200,
            [
                'status' => 'Sukses',
                'message' => [
                    'title' => 'Berhasil',
                    'body' => "Berhasil menghapus data"
                ]
            ]
        );
    }
    protected function responseJSON($status, $response)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($status)
            ->set_output(json_encode($response));
    }

    private function renderTo($filepath, $data = null)
    {
        $this->load->view('template/head.php');
        $this->load->view('template/sidebar.php');
        $this->load->view($filepath, $data);
        $this->load->view('template/footer.php');
    }
}
